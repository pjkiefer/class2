
let Paul = {
    "firstName":"Paul",
    "lastName":"Kiefer",
    "favoriteFood":"Pizza",
    "mother":{
        "firstName":"Mary",
        "lastName:":"Kiefer",
        "favoriteFood":"Cake"
    },
    "father":{
        "firstName":"Charles",
        "lastName:":"Kiefer",
        "favoriteFood":"Steak"
    }
};

console.log(Paul.father.firstName);  //Charles
console.log(Paul.mother.favoriteFood);  //Cake


let tictactoe = [["-","O","-"],["-","X","O"],["X","-","X"]];
//Updates for missing code
tictactoe[0][2] = 'O';
console.log(`${tictactoe[0][0]} ${tictactoe[0][1]} ${tictactoe[0][2]}`); // - O O
console.log(`${tictactoe[1][0]} ${tictactoe[1][1]} ${tictactoe[1][2]}`); // - X O
console.log(`${tictactoe[2][0]} ${tictactoe[2][1]} ${tictactoe[2][2]}`); // X - X


let assignmentDate = '1/21/2019';
let tmp = assignmentDate.split('/');  // ["1", "21", "2019"]
let month = parseInt(tmp[0] - 1); // 0
let day = parseInt(tmp[1]); // 21
let year = parseInt(tmp[2]); // 2019
let myNewDate = new Date(year,month,day); // Mon Jan 21 2019 00:00:00 GMT-0800 (Pacific Standard Time)

let myFutureDate = new Date(year,month,day); // ["1", "21", "2019"]
let daysToAdd = 7; // 7
myFutureDate.setDate(myFutureDate.getDate() + daysToAdd); // Mon Jan 28 2019 00:00:00 GMT-0800 (Pacific Standard Time)

let strYear = myFutureDate.getFullYear().toString(); // 2019
let strMonth = (myFutureDate.getMonth() + 1).toString().padStart(2,'0'); // 01
let strDay = myFutureDate.getDate().toString().padStart(0,'0'); // 28
let strFullDate = `${strYear}-${strMonth}-${strDay}`; // "2019-01-28"

//function myFunction(dte) {
//  var tmp = document.createElement("time");
//  var myText = document.createTextNode(dte);
//  tmp.appendChild(myText);
//  document.body.appendChild(tmp);
//}

let timeTag = '<time datetime="2018-01-14">January 14, 2018</time>'

